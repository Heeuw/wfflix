<?php

/**
 * Created by: Stephan Hoeksema 2022
 * wfflix-cd
 */
class UserModel
{
    protected $conn;
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function showAll()
    {
        $stmt = $this->conn->prepare("SELECT * FROM users");
        $stmt->execute();
        return $stmt->fetchAll();
    }
}